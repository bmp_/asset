/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 *
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */
package id.io.asset.service;

import id.io.asset.model.User;
import id.io.asset.util.constant.ConstantHelper;
import id.io.asset.util.database.UserDatabaseHelper;
import id.io.common.logging.AppLogger;

import java.util.List;
import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;

public class UserService {

    public JSONObject getUsers() {
        AppLogger.info("UserService", "getUsers");
        List<User> userList = UserDatabaseHelper.getUsers();

        JSONArray userArr = new JSONArray(userList);
        JSONObject response = new JSONObject();
        response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_ACCEPTED);
        response.put(ConstantHelper.HTTP_RESPONSE, userList);

        return response;
    }

}
