/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 *
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */
package id.io.asset.resource;

import id.io.asset.service.UserService;
import id.io.asset.util.constant.ConstantHelper;
import id.io.common.logging.AppLogger;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

@Path("user")
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {

    private UserService userService = new UserService();

    @GET
    public Response getUsers() {
        AppLogger.debug("GET api/user");

        JSONObject response = new JSONObject();
        try {
            response = userService.getUsers();
            response.remove(ConstantHelper.HTTP_CODE);
            return Response.ok(response.get(ConstantHelper.HTTP_RESPONSE).toString()).build();
        } catch (JSONException ex) {
            response.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.put(ConstantHelper.HTTP_REASON, "error_create_user_levels");
            response.put(ConstantHelper.HTTP_MESSAGE, "Error Create User Level cause :" + ex.getMessage());
            return Response.ok((!response.has(ConstantHelper.HTTP_CODE))
                    ? HttpStatus.SC_OK : response.getInt(ConstantHelper.HTTP_CODE)).entity(response.toString()).build();

        }

    }

}
