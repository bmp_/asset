/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 *
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */
package id.io.asset.resource;

import id.io.asset.util.constant.ConstantHelper;
import id.io.asset.util.helper.DateHelper;
import id.io.common.logging.AppLogger;
import java.time.LocalDateTime;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.http.HttpStatus;
import org.json.JSONObject;

@Path("ping")
@Consumes(MediaType.APPLICATION_JSON)
public class Ping  {
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response get() {
        AppLogger.info("GET /ping");
        
        JSONObject objResponse = new JSONObject();
        objResponse.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_OK);
        objResponse.put(ConstantHelper.HTTP_MESSAGE, "Server Test Successfull!" );
        
         return Response.status((!objResponse.has(ConstantHelper.HTTP_CODE))
                ? HttpStatus.SC_OK : objResponse.getInt(ConstantHelper.HTTP_CODE)).entity(objResponse.toString()).build();
        

    }
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response post() {
        AppLogger.info("POST /ping");
        
        JSONObject objResponse = new JSONObject();
        objResponse.put(ConstantHelper.HTTP_CODE, HttpStatus.SC_OK);
        objResponse.put(ConstantHelper.HTTP_MESSAGE, "Server Test Successfull!" );
        
         return Response.status((!objResponse.has(ConstantHelper.HTTP_CODE))
                ? HttpStatus.SC_OK : objResponse.getInt(ConstantHelper.HTTP_CODE)).entity(objResponse.toString()+ " | "+ DateHelper.formatDateTime(LocalDateTime.now())).build();
        

    }
    

}
