/*
 * /**
 *   * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *   *
 *   * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 *   * as indicated by the @author tags. All Rights Reserved
 *   *
 *   * The contents of this file are subject to the terms of the
 *   * Common Development and Distribution License (the License).
 *   *
 *   * Everyone is permitted to copy and distribute verbatim copies
 *   * of this license document, but changing it is not allowed.
 *   *
 */
package id.io.asset.init;

import id.io.asset.manager.ConnectionManager;
import id.io.asset.manager.EncryptionManager;
import id.io.asset.util.constant.ConstantHelper;
import id.io.common.logging.AppLogger;
import id.io.common.manager.PropertyManager;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;




@WebListener
public class ContextListener implements ServletContextListener {

    public final AppLogger log;

    public ContextListener() {
        log = new AppLogger(this.getClass());
    }

    @Override
    public void contextInitialized(ServletContextEvent evt) {
        log.info("Application Init Started");
        PropertyManager.init(ConstantHelper.CONFIG_FILE);
        EncryptionManager.init();
        ConnectionManager.getInstance();
        log.info("Application Init Completed");
    }

    @Override
    public void contextDestroyed(ServletContextEvent evt) {
        log.info("Application Shutdown Started");
        ConnectionManager.getInstance().shutdown();
        EncryptionManager.shutdown();
        log.info("Application Shutdown Completed");
    }

}
