/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2019 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 *
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */
package id.io.asset.util.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.io.common.logging.AppLogger;
import id.io.asset.util.string.StringHelper;

public class JsonHelper {

    private static final ObjectMapper OBJ_MAPPER = new ObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL)
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).configure(
            DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);

    public JsonHelper() {

    }

    public static ObjectMapper getMapper() {
        return OBJ_MAPPER;
    }

    public static String toJson(Object obj) {
        try {
            return OBJ_MAPPER.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            AppLogger.error("JsonHelper - toJson", e);
        }
        return "";
    }

    public static <T> T fromJson(String json, Class<T> clazz) {
        final String methodName = "fromJson";
        try {
            return OBJ_MAPPER.readValue(json, clazz);
        } catch (IOException e) {
            AppLogger.error("JsonHelper - " + methodName, e);
        }

        try {
            return clazz.getConstructor().newInstance();
        } catch (Exception ex) {
            AppLogger.error("JsonHelper - " + methodName, "Could not invoke Default Constructor", ex);
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public static <T> List<T> fromJsonArray(String json, Class<T> clazz) {
        final String methodName = "fromJsonArray";
        try {
            Class<T[]> arrayClass = (Class<T[]>) Class.forName("[L" + clazz.getName() + ";");
            T[] objects = OBJ_MAPPER.readValue(json, arrayClass);
            return Arrays.asList(objects);
        } catch (ClassNotFoundException | IOException e) {
            AppLogger.error("JsonHelper - " + methodName, e);
        }

        return new ArrayList<>();

    }

    public static JSONObject mapResponseJson(String message) {
        JSONObject msg = new JSONObject();
        msg.put("message", message.startsWith("error_") ? StringHelper.mapErrorMessage(message) : message);
        msg.put("reason", message);
        msg.put("code", 400);
        return msg;
    }
}
