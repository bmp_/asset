/*
 * /**
 *   * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *   *
 *   * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 *   * as indicated by the @author tags. All Rights Reserved
 *   *
 *   * The contents of this file are subject to the terms of the
 *   * Common Development and Distribution License (the License).
 *   *
 *   * Everyone is permitted to copy and distribute verbatim copies
 *   * of this license document, but changing it is not allowed.
 *   *
 */
package id.io.asset.util.database;

import id.io.common.logging.AppLogger;
import id.io.asset.manager.ConnectionManager;

import java.sql.SQLException;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.PreparedBatch;
import org.jdbi.v3.core.statement.Update;

public class BaseDatabaseHelper {

    protected static AppLogger log;

    protected AppLogger getLogger(Class<?> clazz) {
        return new AppLogger(clazz);
    }

    protected static Handle getHandle() throws SQLException {
        return ConnectionManager.getInstance().getHandle();
    }

    protected static Handle getHandle(RowMapper<?>... rowMappers) throws SQLException {
        Handle h = getHandle();

        if (rowMappers != null && rowMappers.length > 0) {
            for (RowMapper<?> mapper : rowMappers) {
                h.registerRowMapper(mapper);
            }
        }
        return h;
    }

    protected static boolean executeUpdate(Update update) {
        return update.execute() > 0;
    }

    protected static int executeAndGetId(Update update) {
        return update.executeAndReturnGeneratedKeys().mapTo(Integer.class).one();
    }

    protected static boolean executeBatch(PreparedBatch batch) {
        int[] resultArr = batch.execute();

        for (int result : resultArr) {
            if (result < 0) {
                return false;
            }
        }
        return true;
    }

    protected static void start(String methodName) {
        log.debug(methodName, "start");
    }

    protected static void completed(String methodName) {
        log.debug(methodName, "completed");
    }

}
