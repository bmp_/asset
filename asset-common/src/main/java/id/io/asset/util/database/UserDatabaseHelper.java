/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2020 IO-Teknologi Indonesia, and individual contributors
 * as indicated by the @author tags. All Rights Reserved
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 *
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 *
 */
package id.io.asset.util.database;

import id.io.asset.model.User;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.jdbi.v3.core.Handle;

public class UserDatabaseHelper extends BaseDatabaseHelper {
    
     public static List<User> getUsers() {
        String methodName = "getUsers";
        List<User> userList = new ArrayList<>();
        start(methodName);

        final String sql = "SELECT manageduser.userid, manageduser.username, usermember.givenname, usermember.fullname, usermember.email, "
                + "usermember.phone, usermember.avatar, usermember.levelid, useraddress.address, useraddress.zone, useraddress.geolocation, manageduser.isactive, "
                + "usermember.createdt  FROM manageduser INNER JOIN usermember ON manageduser.memberid=usermember.memberid INNER JOIN useraddress ON usermember.addressid=useraddress.addressid;";
        try (Handle handle = getHandle()) {
            userList = handle.createQuery(sql).mapToBean(User.class).list();
        } catch (SQLException ex) {
            log.error(UserDatabaseHelper.class.getName(), " - errorListUser : " + ex);
        }

        completed(methodName);
        return userList;
    }
    
}
